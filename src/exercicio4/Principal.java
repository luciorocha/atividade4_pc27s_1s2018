/**
 * Exercício de Semáforo com BufferSincronizado: Modelo Produtor/Consumidor
 * 
 * Sem utilizar ArrayBlockingQueue
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 21/03/2018
 */
package exercicio4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Principal {

    public Principal(){
        ExecutorService pool = Executors.newCachedThreadPool();
        
        Buffer bufferCompartilhado = new BufferSincronizado();
        
        //Cria a thread dentro do pool, com o bufferCompartilhado
        PrintTasks t1 = new PrintTasks(bufferCompartilhado, "Sinal1");
        PrintTasks t2 = new PrintTasks(bufferCompartilhado, "Sinal2");
        PrintTasks t3 = new PrintTasks(bufferCompartilhado, "Sinal3");
        
        pool.execute ( t1 );
        pool.execute ( t2 );
        pool.execute ( t3 );
        
        //Nao aceita mais submissões de threads, e finaliza
        pool.shutdown();
    }
    
    public static void main(String [] args){
        new Principal();
    }
    
}//fim classe

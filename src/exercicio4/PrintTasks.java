/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exercicio4;

import java.util.Random;

/**
 *
 * @author Lucio
 */
public class PrintTasks implements Runnable {

    //Tempo aleatorio em que cada thread entra em sleep
    private final static Random generator = new Random();
    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa    

    private int cor = 1;
    private int corBuffer = 2;
    private final Buffer buffer;
    
    public PrintTasks( Buffer shared , String name){
        buffer = shared;
        
        this.taskName = name;

        this.sleepTime = 2000; //milissegundos
    }
    
    public void run() {
        try {
            buffer.set(cor, taskName);
            while (true) {
                //System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
                switch (cor) {
                    case 1:
                        //Tenho: verde                        
                            buffer.set( cor++, taskName );  //todo set tem que ser acompanhado de um notifyAll                          
                        break;
                    case 2:
                            buffer.set( 1, taskName );                        
                        break;

                    default:
                        break;
                }
                //Estado de ESPERA SINCRONIZADA
                //Nesse ponto, a thread perde o processador, e permite que
                //outra thread execute
                //
                cor = buffer.get(taskName);
                Thread.sleep(generator.nextInt(300)); //Deixa os carros passarem
                //Thread.sleep(2000);
                
            }//fim while

        } catch (Exception ex) {
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
        
    }//fim run

}//fim classe

/**
 * Exemplo de BufferSincronizado com métodos
 * wait(), notify() e notifyAll()
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 19/10/2017
 */
package exercicio4;

import java.util.Random;

/**
 *
 * @author Lucio
 */
public class BufferSincronizado implements Buffer {

        //Tempo aleatorio em que cada thread entra em sleep
    private final static Random generator = new Random();
    
    private int buffer = -1; // compartilhado por todas as threads
    private boolean temValor = false; //indica se o buffer temValor
    
    //Insere o valor no buffer
    public synchronized void set(int valor, String taskName) throws InterruptedException{
        
        //Enquanto nao houver posicoes vazias, 
        //coloca a thread em estado de Espera
        if ( temValor ){
            System.out.println("---");
            //System.out.println(taskName+" Vermelho");
            System.out.println(taskName + " Thread tenta escrever.");
            System.out.println("Buffer cheio. Thread espera. | Buffer: " + buffer + " temValor: " + temValor);                       
            System.out.println("---set---");
            
            //wait();
            //Thread.sleep(1000);
            Thread.sleep(generator.nextInt(300));
        } else {
        
        buffer = valor; //insere um valor no buffer
        
        //Indica que o produtor não pode armazenar outro valor ate que
        //o consumidor recupere o valor do buffer.
        temValor = true; 
        
        System.out.println("---");
        System.out.println(taskName + " Verde ");
        System.out.println(taskName + " Thread escreve: " + buffer+ " | Buffer: " + buffer + " temValor: " + temValor);
        //System.out.println(taskName + " Vermelho ");
        System.out.println("---set---");
        
        //Informa a todas as outras threads em espera para
        //entrarem no estado Executável
        //notifyAll(); 
        }
    }
    
    public synchronized int get(String taskName) throws InterruptedException{
    
        if (!temValor){
            System.out.println("---");
            System.out.println(taskName + " Thread tenta ler.");
            System.out.println("Buffer vazio. Thread espera. | Buffer: " + buffer + " temValor: " + temValor);
            
            //System.out.println(taskName+" Vermelho");
            System.out.println("---get---");
            //Entra em espera ate que a thread seja notificada
            //Thread.sleep(1000);
            Thread.sleep(generator.nextInt(300));
            //wait();
        } else {
        
        //Consumidor acabou de recuperar valor do buffer.
        //Produtor pode inserir outro valor
        temValor=false;
        
        System.out.println("---");        
        System.out.println(taskName + " Thread lê: " + buffer + " | Buffer: " + buffer + " temValor: " + temValor);
        System.out.println(taskName + " Vermelho ");        
        System.out.println("---get---");
        //if (buffer<4)
        //    buffer++;
        //else
        //    buffer=1;
        //Informa a todas as outras threads em espera para
        //entrarem no estado Executável
        //notifyAll();
        }
        
        return buffer;
        
        
    }//fim get
    
}//fim classe

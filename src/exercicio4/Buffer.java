/**
 * Buffer
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 19/10/2017
 */
package exercicio4;

public interface Buffer {

    public void set (int value, String taskName )throws InterruptedException;
    
    public int get ( String taskName ) throws InterruptedException;
    
}
